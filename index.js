function guardarEnLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  localStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  localStorage.setItem("json", JSON.stringify(objeto));
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Clave " + clave + " guardado";
}

function guardarEnSessionStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var txtValor = document.getElementById("txtValor"); /* Referencia al input de valor */
  var clave = txtClave.value;
  var valor = txtValor.value;
  sessionStorage.setItem(clave, valor);
  var objeto = {
    nombre:"Ezequiel",
    apellidos:"Llarena Borges",
    ciudad:"Madrid",
    pais:"España"
  };
  sessionStorage.setItem("json", JSON.stringify(objeto));
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Clave " + clave + " guardado";
}

function leerDeLocalStorage() {
  var txtClave = document.getElementById("txtClave"); /* Referencia al input de clave */
  var clave = txtClave.value;
  var valor = localStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(localStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function leerDeSessionStorage() {
  var txtClave = document.getElementById("txtClave");
  var clave = txtClave.value;
  var valor = sessionStorage.getItem(clave);
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = valor;
  var datosUsuario = JSON.parse(sessionStorage.getItem("json"));
  console.log(datosUsuario.nombre);
  console.log(datosUsuario.pais);
  console.log(datosUsuario);
}

function limpiarSessionStorage(){
  sessionStorage.clear();
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Valores de sesion limpiados";
}

function tamanoSessionStorage(){
  var tam = sessionStorage.length();
  var spanValor = document.getElementById("spanValor");
  spanValor.innerText = "Tamaño: " + sessionStorage.length;
}

function eliminarDeSessionStorage() {
  var txtClave = document.getElementById("txtClave");
  var clave = txtClave.value;
  sessionStorage.removeItem(clave);
  spanValor.innerText = "Clave " + clave + " eliminado";
}
